public class Packet_Defines_Server {
    public static final short LOGIN_SUCCESS=1000;
    public static final short LOGIN_FAIL_CREDENTIALS=1001;
    public static final short LOGIN_FAIL_SERVICE=1002;
    public static final short LOGIN_FAILED_USER_ALREADY_LOGGED=1021;

    public static final short REGISTER_SUCCESS=1004;
    public static final short REGISTER_FAIL_USER_DUPLICATE=1005;
    public static final short REGISTER_FAIL_SERVICE=1006;


    public static final short GET_FRIEND_LIST_SUCCESS=1007;
    //public static final short GET_FRIEND_LIST_FAILED_NO_FRIENDS_ONLINE=1015;
    public static final short GET_FRIEND_LIST_FAILED_SERVICE=1008;
    public static final short GET_FRIEND_LIST_FAILED_INVALID_TOKEN=1009;
    public static final short GET_FRIEND_LIST_FAILED_NO_FRIENDS=1020;


    //public static final short SEND_MSG_SUCESS=1010;
    public static final short SEND_MSG_FAIL_USER_OFFLINE=1011;
    public static final short SEND_MSG_FAIL_INVALID_TOKEN=1012;
    public static final short SEND_MSG_FAIL_USER_NOT_IN_FRIENDLIST=1018;
    public static final short SEND_MSG_FAIL_SERVICE=1019;

    public static final short RECEIVED_MSG_SUCCESS=1013;

    public static final short GET_TOKEN=1014;

    public static final short INVALID_CLIENT_PACKET=1016;

    public static final short CONFIRM_DISCONNECT=1022;


    public static final short ADD_FRIEND_USER_ALREADY_FRIEND=1023;
    public static final short ADD_FRIEND_USER_NOT_ONLINE=1024;
    public static final short ADD_FRIEND_USER_FAIL_SERVICE=1025;

    public static final short SELF_MESSAGE=1026;
}
