import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Chat_Utilities {



    public static String randomizeString(String input){

        List<Character> characters = new ArrayList<Character>();
        for(char c:input.toCharArray()){
            characters.add(c);
        }
        StringBuilder output = new StringBuilder(input.length());
        while(characters.size()!=0){
            int randPicker = (int)(Math.random()*characters.size());
            output.append(characters.remove(randPicker));
        }

        return output.toString();
    }


    public static void sendBytes(byte[] myByteArray, int start, int len, Socket socket) throws IOException {
        if (len < 0)
            throw new IllegalArgumentException("Negative length not allowed");
        if (start < 0 || start >= myByteArray.length)
            throw new IndexOutOfBoundsException("Out of bounds: " + start);
        OutputStream out = socket.getOutputStream();
        DataOutputStream dos = new DataOutputStream(out);

        dos.writeInt(len);

        if (len > 0) {
            dos.write(myByteArray, start, len);
        }
    }


    public static byte[] readBytes(Socket socket) throws IOException {

        InputStream in = socket.getInputStream();
        DataInputStream dis = new DataInputStream(in);

        if(dis.available()>0) {

            int len = dis.readInt();
            byte[] data = new byte[len];

            if (len > 0) {
                dis.readFully(data);
                return data;
            } else return null;
        }else return null;
    }

    public static byte[] stringToBytesUTFCustom(String str) {
        char[] buffer = str.toCharArray();
        byte[] b = new byte[buffer.length << 1];
        for(int i = 0; i < buffer.length; i++) {
            int bpos = i << 1;
            b[bpos] = (byte) ((buffer[i]&0xFF00)>>8);
            b[bpos + 1] = (byte) (buffer[i]&0x00FF);
        }
        return b;
    }

    public static String bytesToStringUTFCustom(byte[] bytes) {
        char[] buffer = new char[bytes.length >> 1];
        for(int i = 0; i < buffer.length; i++) {
            int bpos = i << 1;
            char c = (char)(((bytes[bpos]&0x00FF)<<8) + (bytes[bpos+1]&0x00FF));
            buffer[i] = c;
        }
        return new String(buffer);
    }

    public static byte[] copyByteArray(byte[] arrSource,byte[]arrDest, int startSource, int lenSource,int startDest)
    {
        for(int i=0;i<lenSource;i++) {
            arrDest[startDest+i]=arrSource[startSource+i];

        }
        return arrDest;
    }

    public static byte[] cutArray(byte[] arrSource, int startSource, int lenCut)
    {
        byte[]returnByteArr = new byte[lenCut];

        for(int i=0;i<lenCut;i++)
        {
            returnByteArr[i]=arrSource[startSource+i];
        }

        return returnByteArr;


    }

    public static void printbytes(byte[]byteArray)
    {
        for(int i=0;i<byteArray.length;i++)
            System.out.print(byteArray[i]+";");



    }

}
