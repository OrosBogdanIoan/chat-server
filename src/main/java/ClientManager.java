import com.sun.deploy.util.SessionState;
import com.sun.javaws.exceptions.InvalidArgumentException;

import javax.print.DocFlavor;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ClientManager extends Thread {

    private ArrayList<Socket> clients;
    private LinkedList<Promise<SocketPacketStruct,SocketPacketStruct[]>> packetsQueue;
    private HashMap<Socket,String> tokenSocketMap;
    private HashMap<String,Socket> userSocketMap;
    private HashMap<Socket,String> socketUserMap;
    private boolean isRunning;
    private ResponseGenerator responseGenerator;

    public void terminate() {
        isRunning = false;
    }


    public ClientManager( ArrayList<Socket> clients) {
        this.packetsQueue=new LinkedList<Promise<SocketPacketStruct,SocketPacketStruct[]>>();
        this.clients = clients;
        isRunning = true;
        tokenSocketMap= new HashMap<Socket,String>();
        userSocketMap = new HashMap<String, Socket>();
        socketUserMap = new HashMap<Socket,String>();
    }



    @Override
    public void run() {

        byte[] content;

        // Create 4-threaded threadpool
        ExecutorService executor = Executors.newFixedThreadPool(4);

        System.out.println("Thread pool initialized");

        DBManager dbManager = new DBManager("jdbc:sqlserver://localhost:1433;DatabaseName=Chat", "sa","bog18dy");




            while (isRunning) {

            synchronized (clients)
            {

                //Read data and pass packet processing work to worker thread pool
                for (Socket s : clients)
                {


                        try
                        {

                            if ((content = Chat_Utilities.readBytes(s)) != null) //Non-blocking read
                            {
                                SocketPacketStruct received = new SocketPacketStruct(s,content);

                                Future<SocketPacketStruct[]> r = executor.submit(new ResponseGenerator( received,dbManager,tokenSocketMap,userSocketMap,socketUserMap));

                                packetsQueue.add(new Promise(received,r));
                                System.out.println("[PROMISE REQUEST] TYPE - " +received.getPacket().getType() + " FROM " + received.getClientsocket());
                            }


                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                        }

                }

                //Check for any "promises" that are done and send those packets


                for(Promise promise : packetsQueue)
                {
                    if(promise.isFullfilled()) // Non blocking check for promise finish
                    {
                        System.out.println("[PROMISE DONE] - "+ ((SocketPacketStruct)promise.getWish()).getPacket().getType()+" FROM "+((SocketPacketStruct)promise.getWish()).getClientsocket());
                        try {
                            for(final SocketPacketStruct  packet : (SocketPacketStruct[]) promise.getResults().get())
                            {
                                if(((SocketPacketStruct)promise.getWish()).getPacket().getType()== Packet_Defines_Client.REQUEST_LOGIN && packet.getPacket().getType()==Packet_Defines_Server.LOGIN_SUCCESS  )
                                {
                                    userSocketMap.put(((SocketPacketStruct)promise.getWish()).getPacket().getMsg()[0],((SocketPacketStruct)promise.getWish()).getClientsocket());
                                    socketUserMap.put(((SocketPacketStruct)promise.getWish()).getClientsocket(),((SocketPacketStruct)promise.getWish()).getPacket().getMsg()[0]);
                                    System.out.println("[LOGGED IN]" +((SocketPacketStruct)promise.getWish()).getClientsocket() +((SocketPacketStruct)promise.getWish()).getPacket().getMsg()[0] );
                                }

                                if(((SocketPacketStruct)promise.getWish()).getPacket().getType()== Packet_Defines_Client.REQUEST_LOGIN && packet.getPacket().getType()==Packet_Defines_Server.GET_TOKEN  ) {

                                tokenSocketMap.put(((SocketPacketStruct)promise.getWish()).getClientsocket(),packet.getPacket().getMsg()[0]);
                                System.out.println("[AUTHENTICATED]" +((SocketPacketStruct)promise.getWish()).getClientsocket() +" with token " + packet.getPacket().getMsg()[0] );
                                }

                                if(packet.getPacket().getType()==Packet_Defines_Server.CONFIRM_DISCONNECT)
                                {
                                    System.out.println("[DISCONNECTED] USER " + socketUserMap.get(packet.getClientsocket())+ " WITH AUTH TOKEN "+tokenSocketMap.get(packet.getClientsocket())+" AND SOCKET "+ packet.getClientsocket());
                                    tokenSocketMap.remove(packet.getClientsocket());
                                    userSocketMap.remove(socketUserMap.get(packet.getClientsocket()));
                                    socketUserMap.remove(packet.getClientsocket());
                                }

                                Chat_Utilities.sendBytes(packet.getPacket().encodeBinary(),0,packet.getPacket().encodeBinary().length,packet.getClientsocket());

                            }



                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        finally {
                            packetsQueue.remove(promise);// Promise fullfilled, packets send, remove  promise
                        }

                    }


                }


            }
        }


        executor.shutdown();
        System.out.println("Worker ThreadPool terminated");
    }
}
