import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DBManager {
    private final String db_connect_string;
    private final String db_userid;
    private final String db_password;

    public DBManager(String db_connect_string, String db_userid, String db_password) {
        this.db_connect_string = db_connect_string;
        this.db_userid = db_userid;
        this.db_password = db_password;
    }


    public ResultSet  query(String query, byte type)
    {
        Statement statement;
        Connection conn;
        ResultSet rs=null;

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(db_connect_string, db_userid, db_password);
            statement = conn.createStatement();

            if(type==0)
            rs= statement.executeQuery(query);
            else statement.executeQuery(query);


        } catch (Exception e) {

           // e.printStackTrace();

        }
        finally {
            return rs;
        }



    }


}
