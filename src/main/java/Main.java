import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;


public class Main {
    private static ClientManager clientManager;
    private static CommandListener cmdListener;
    private static ServerSocket serverSocket;
    private static ArrayList<Socket> clients;

    public static void main(String[] args) {

        boolean isRunning=true;
        cmdListener=null;

        clients = new ArrayList<Socket>();

        //Receive/Send packets thread
        clientManager = new ClientManager(clients);
        clientManager.start();
        System.out.println("Receive/Send Thread Started");


        try
        {
            serverSocket = new ServerSocket(4449);
            cmdListener = new CommandListener(serverSocket);
            cmdListener.start();

        while (isRunning) {
            try {

               Socket socket = serverSocket.accept();

                synchronized (clients)
                {
                    clients.add(socket);
                }

            }
            catch (IOException e)
            {
                isRunning=false;
                System.out.println("I/O error: " + e);
            }



        }


        }
        catch(IOException ex)
        {
            System.out.println("Server setup failed :" + ex.toString());
        }


        cmdListener.terminate();

        clientManager.terminate();
        System.out.println("Send/Receive Thread terminated");

        System.out.println("Main (Accept) Thread terminated");

    }



}
