import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;

class CommandListener extends Thread {

    private final ServerSocket svrSocket;
    private boolean isRunning;

    public CommandListener(ServerSocket svrSocket) {
        this.svrSocket = svrSocket;
        isRunning=true;
    }

    public void terminate()
    {
        isRunning=false;
    }

    @Override
    public void run() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while(isRunning) {
            try {
                String command = br.readLine();
                if(command.equals("quit")) {
                    svrSocket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}