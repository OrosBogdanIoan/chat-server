import com.sun.javaws.exceptions.InvalidArgumentException;

import java.net.Socket;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Bidi;
import java.util.*;
import java.util.concurrent.Callable;

import static java.lang.StrictMath.abs;

public class ResponseGenerator implements Callable {


    private final SocketPacketStruct receivedPacket;
    private final DBManager dbManager;
    private final HashMap<Socket,String> tokenSocketMap;
    private final HashMap<String,Socket> userSocketMap;
    private final HashMap<Socket,String> socketUserMap;

    public ResponseGenerator(SocketPacketStruct receivedPacket, DBManager dbManager, HashMap<Socket, String> tokenSocketMap, HashMap<String, Socket> userSocketMap, HashMap<Socket, String> socketUserMap) {
        this.receivedPacket = receivedPacket;
        this.dbManager = dbManager;
        this.tokenSocketMap = tokenSocketMap;
        this.userSocketMap = userSocketMap;
        this.socketUserMap = socketUserMap;
    }

    public SocketPacketStruct[] call() {
        SocketPacketStruct[] replyPacket;
        Packet responsePacket = null;
        Random random = new Random();
        String token;
        Date date = new Date();



            switch (receivedPacket.getPacket().getType()) {

                case Packet_Defines_Client.REQUEST_LOGIN: {

                    if(userSocketMap.keySet().contains(receivedPacket.getPacket().getMsg()[0]))
                    {

                        replyPacket = new SocketPacketStruct[1];
                        replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(),new Packet(Packet_Defines_Server.LOGIN_FAILED_USER_ALREADY_LOGGED).encodeBinary());
                        return replyPacket;
                    }


                    try {

                       ResultSet r = dbManager.query("SELECT COUNT(password) AS row_count FROM dbo.Member_Credentials WHERE username='".concat(receivedPacket.getPacket().getMsg()[0]).concat("';"),CRUD_OPERATIONS.SELECT);

                        r.next();
                        int count = r.getInt("row_count");

                        if (count > 0)
                        {
                            ResultSet rs = dbManager.query("SELECT password FROM dbo.Member_Credentials WHERE username='".concat(receivedPacket.getPacket().getMsg()[0]).concat("';"),CRUD_OPERATIONS.SELECT);


                            while (rs.next())
                            {

                                if (rs.getString("password").equals(receivedPacket.getPacket().getMsg()[1]))
                                {
                                    replyPacket = new SocketPacketStruct[2];
                                    token = String.valueOf(abs(random.nextInt() % 100)) + Chat_Utilities.randomizeString(receivedPacket.getPacket().getMsg()[0]) + String.valueOf(date.getTime() % 100);
                                    replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(),new Packet(Packet_Defines_Server.LOGIN_SUCCESS).encodeBinary());
                                    replyPacket[1] = new SocketPacketStruct(receivedPacket.getClientsocket(),new Packet(Packet_Defines_Server.GET_TOKEN,(short)1,new String[]{token}).encodeBinary());
                                    return replyPacket;

                                } else {
                                    replyPacket = new SocketPacketStruct[1];
                                    replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(),new Packet(Packet_Defines_Server.LOGIN_FAIL_CREDENTIALS).encodeBinary());
                                    return replyPacket;
                                }
                            }
                        } else
                            {
                                replyPacket = new SocketPacketStruct[1];
                                replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(),new Packet(Packet_Defines_Server.LOGIN_FAIL_CREDENTIALS).encodeBinary());
                                return replyPacket;
                            }


                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        replyPacket = new SocketPacketStruct[1];
                        replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(),new Packet(Packet_Defines_Server.LOGIN_FAIL_SERVICE).encodeBinary());
                        return replyPacket;
                    }


                }
                case Packet_Defines_Client.REQUEST_REGISTER: {
                    //Not implemented
                    replyPacket = new SocketPacketStruct[1];



                    ResultSet r = dbManager.query("SELECT COUNT(password) AS row_count FROM dbo.Member_Credentials WHERE username='".concat(receivedPacket.getPacket().getMsg()[0]).concat("';"),CRUD_OPERATIONS.SELECT);

                    try {
                        r.next();
                        int count = r.getInt("row_count");
                        if (count > 0) {

                            replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(),new Packet(Packet_Defines_Server.REGISTER_FAIL_USER_DUPLICATE).encodeBinary());

                        }
                        else
                        {
                            dbManager.query("INSERT INTO dbo.Member_Credentials(username,password) VALUES('" + receivedPacket.getPacket().getMsg()[0] + "','" + receivedPacket.getPacket().getMsg()[1] + "');",CRUD_OPERATIONS.INSERT);

                            replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(),new Packet(Packet_Defines_Server.REGISTER_SUCCESS).encodeBinary());
                        }

                    }
                    catch (SQLException e) {
                        //e.printStackTrace();
                        replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(),new Packet(Packet_Defines_Server.REGISTER_FAIL_SERVICE).encodeBinary());

                    }
                    finally
                    {
                        return replyPacket;
                    }



                }
                case Packet_Defines_Client.REQUEST_FRIEND_LIST: { //TOKEN
                    //Not implemented

                    try{


                    if(!(tokenSocketMap.containsValue(receivedPacket.getPacket().getMsg()[0]) && tokenSocketMap.containsKey(receivedPacket.getClientsocket())))
                    {
                        System.out.println("AUTH FAILED WITH TOKEN"+receivedPacket.getPacket().getMsg()[0]+ " AND SOCKET " + receivedPacket.getClientsocket().toString());

                        replyPacket = new SocketPacketStruct[1];
                        replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(), new Packet(Packet_Defines_Server.GET_FRIEND_LIST_FAILED_INVALID_TOKEN).encodeBinary());
                        return replyPacket;

                    }


                     int friendsCount=QueryManager.queryNumberOfFriends(dbManager,socketUserMap.get(receivedPacket.getClientsocket()));


                    if(friendsCount>0)
                    {


                        replyPacket=new SocketPacketStruct[1];
                        replyPacket[0]= new SocketPacketStruct(receivedPacket.getClientsocket(),new Packet(Packet_Defines_Server.GET_FRIEND_LIST_SUCCESS,(short)friendsCount,QueryManager.queryFriendList(dbManager,socketUserMap.get(receivedPacket.getClientsocket()))).encodeBinary());
                        return replyPacket;


                    }
                    else
                    {
                        replyPacket=new SocketPacketStruct[1];
                        replyPacket[0]= new SocketPacketStruct(receivedPacket.getClientsocket(),new Packet(Packet_Defines_Server.GET_FRIEND_LIST_FAILED_NO_FRIENDS).encodeBinary());
                        return replyPacket;

                    }

                    } catch (SQLException e) {
                        e.printStackTrace();
                        replyPacket=new SocketPacketStruct[1];
                        replyPacket[0]= new SocketPacketStruct(receivedPacket.getClientsocket(),new Packet(Packet_Defines_Server.GET_FRIEND_LIST_FAILED_SERVICE).encodeBinary());
                        return replyPacket;
                    }



                } // USER_TO + TOKEN + MSG
                case Packet_Defines_Client.SEND_MSG: {


                    if(!(tokenSocketMap.containsValue(receivedPacket.getPacket().getMsg()[1]) && tokenSocketMap.containsKey(receivedPacket.getClientsocket())))
                    {
                        System.out.println("AUTH FAILED WITH TOKEN"+receivedPacket.getPacket().getMsg()[1]+ " AND SOCKET " + receivedPacket.getClientsocket().toString());

                        replyPacket = new SocketPacketStruct[1];
                        replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(), new Packet(Packet_Defines_Server.SEND_MSG_FAIL_INVALID_TOKEN).encodeBinary());
                        return replyPacket;

                    }



                    if(!userSocketMap.containsKey(receivedPacket.getPacket().getMsg()[0]))
                    {
                        replyPacket = new SocketPacketStruct[1];
                        replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(), new Packet(Packet_Defines_Server.SEND_MSG_FAIL_USER_OFFLINE).encodeBinary());
                        return replyPacket;

                    }
                    try {
                    ResultSet set =  dbManager.query("SELECT      COUNT(  dbo.Member_Credentials.username) as row_count FROM            dbo.FriendList INNER JOIN dbo.Member_Credentials ON dbo.FriendList.user_2 = dbo.Member_Credentials.user_id WHERE dbo.FriendList.user_1= (SELECT user_id from dbo.Member_Credentials where username='"+socketUserMap.get(receivedPacket.getClientsocket())+"') AND dbo.FriendList.user_2= (SELECT user_id from dbo.Member_Credentials where username='"+receivedPacket.getPacket().getMsg()[0]+"');",CRUD_OPERATIONS.SELECT);

                        set.next();


                        if(set.getInt("row_count")==0) {

                            replyPacket = new SocketPacketStruct[1];
                            replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(), new Packet(Packet_Defines_Server.SEND_MSG_FAIL_USER_NOT_IN_FRIENDLIST).encodeBinary());
                            return replyPacket;

                        }


                    } catch (SQLException e) {

                        e.printStackTrace();
                        replyPacket = new SocketPacketStruct[1];
                        replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(), new Packet(Packet_Defines_Server.SEND_MSG_FAIL_SERVICE).encodeBinary());
                        return replyPacket;

                    }



                        replyPacket = new SocketPacketStruct[2];
                        //SEND CONFIRM TO SENDING USER
                        replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(), new Packet(Packet_Defines_Server.SELF_MESSAGE,(short)2,new String[]{receivedPacket.getPacket().getMsg()[0],receivedPacket.getPacket().getMsg()[2]}).encodeBinary());
                        // SEND MSG TO RECEVING USER
                        replyPacket[1] = new SocketPacketStruct(userSocketMap.get(receivedPacket.getPacket().getMsg()[0]), new Packet(Packet_Defines_Server.RECEIVED_MSG_SUCCESS,(short)2,new String[]{socketUserMap.get(receivedPacket.getClientsocket()),receivedPacket.getPacket().getMsg()[2]}).encodeBinary());
                        System.out.println("SEND TO :  " + userSocketMap.get(receivedPacket.getPacket().getMsg()[0] )+ " MSG  : "+receivedPacket.getPacket().getMsg()[2]+" FROM USER " + socketUserMap.get(receivedPacket.getClientsocket()));

                        return replyPacket;


                }

                case Packet_Defines_Client.REQUEST_DISCONNECT:
                {
                    replyPacket = new SocketPacketStruct[1];
                    replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(), new Packet(Packet_Defines_Server.CONFIRM_DISCONNECT).encodeBinary());
                    return replyPacket;

                }

                case Packet_Defines_Client.REQUEST_FRIEND_ADD:
                {
                    //TOKEN AUTH
                    if(!(tokenSocketMap.containsValue(receivedPacket.getPacket().getMsg()[0]) && tokenSocketMap.containsKey(receivedPacket.getClientsocket())))
                    {
                        System.out.println("AUTH FAILED WITH TOKEN"+receivedPacket.getPacket().getMsg()[0]+ " AND SOCKET " + receivedPacket.getClientsocket().toString());

                        replyPacket = new SocketPacketStruct[1];
                        replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(), new Packet(Packet_Defines_Server.SEND_MSG_FAIL_INVALID_TOKEN).encodeBinary());
                        return replyPacket;

                    }
                    //CHECK IF USER ONLINE
                    if(!userSocketMap.keySet().contains(receivedPacket.getPacket().getMsg()[1]))
                    {
                        replyPacket = new SocketPacketStruct[1];
                        replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(), new Packet(Packet_Defines_Server.ADD_FRIEND_USER_NOT_ONLINE).encodeBinary());
                        return replyPacket;
                    }

                    try {

                        //CHECK IF THEY ARE NOT ALREADY FRIENDS
                    boolean isAlreadyFriend=QueryManager.queryIsFriendsWith(dbManager,socketUserMap.get(receivedPacket.getClientsocket()),receivedPacket.getPacket().getMsg()[1]);

                    if(isAlreadyFriend)
                    {
                        replyPacket = new SocketPacketStruct[1];
                        replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(), new Packet(Packet_Defines_Server.ADD_FRIEND_USER_ALREADY_FRIEND).encodeBinary());
                        return replyPacket;
                    }
                    else {

                        dbManager.query("INSERT INTO [Chat].[dbo].[FriendList] VALUES( (SELECT user_id FROM dbo.Member_Credentials WHERE username='" + socketUserMap.get(receivedPacket.getClientsocket()).toString() + "' ), (SELECT user_id FROM dbo.Member_Credentials WHERE username='" + receivedPacket.getPacket().getMsg()[1] + "'));", CRUD_OPERATIONS.INSERT);
                        dbManager.query("INSERT INTO [Chat].[dbo].[FriendList] VALUES( (SELECT user_id FROM dbo.Member_Credentials WHERE username='" + receivedPacket.getPacket().getMsg()[1] + "' ), (SELECT user_id FROM dbo.Member_Credentials WHERE username='" + socketUserMap.get(receivedPacket.getClientsocket()) + "'));",CRUD_OPERATIONS.INSERT);

                        int friendsCountRequestingUser = QueryManager.queryNumberOfFriends(dbManager,socketUserMap.get(receivedPacket.getClientsocket()));
                        int friendsCountSendingUser= QueryManager.queryNumberOfFriends(dbManager,receivedPacket.getPacket().getMsg()[1]);

                        replyPacket = new SocketPacketStruct[2];

                        replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(), new Packet(Packet_Defines_Server.GET_FRIEND_LIST_SUCCESS, (short) friendsCountRequestingUser, QueryManager.queryFriendList(dbManager,socketUserMap.get(receivedPacket.getClientsocket()))).encodeBinary());
                        replyPacket[1] = new SocketPacketStruct(userSocketMap.get(receivedPacket.getPacket().getMsg()[1]), new Packet(Packet_Defines_Server.GET_FRIEND_LIST_SUCCESS, (short) friendsCountSendingUser, QueryManager.queryFriendList(dbManager,receivedPacket.getPacket().getMsg()[1])).encodeBinary());

                        return replyPacket;


                    }

                    } catch (SQLException e) {
                        replyPacket = new SocketPacketStruct[1];
                        replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(), new Packet(Packet_Defines_Server.ADD_FRIEND_USER_FAIL_SERVICE).encodeBinary());
                        return replyPacket;
                    }




                }


                default: {
                    replyPacket = new SocketPacketStruct[1];
                    replyPacket[0] = new SocketPacketStruct(receivedPacket.getClientsocket(), new Packet(Packet_Defines_Server.INVALID_CLIENT_PACKET).encodeBinary());
                    return replyPacket;
                }


            }


    }


}
