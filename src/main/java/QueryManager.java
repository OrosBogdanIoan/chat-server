import java.sql.ResultSet;
import java.sql.SQLException;

public class QueryManager {

    public static String[] queryFriendList(DBManager dbManager,String username) throws SQLException {
        int friendsCount=queryNumberOfFriends(dbManager,username);
        if(friendsCount==0) return null;

        ResultSet rs = dbManager.query("SELECT dbo.Member_Credentials.username as userr FROM  dbo.FriendList INNER JOIN dbo.Member_Credentials ON dbo.FriendList.user_2 = dbo.Member_Credentials.user_id WHERE dbo.FriendList.user_1= (SELECT user_id from dbo.Member_Credentials where username='"+username+"');",CRUD_OPERATIONS.SELECT);
        String[] friends=new String[friendsCount];
        int count=0;


            while(rs.next())
            {
                String person = rs.getString("userr");
                friends[count]=person;
                count++;
                System.out.println("[FriendList]"+ username +"is friends with " + person);
            }

            return friends;




    }

    public static int queryNumberOfFriends(DBManager dbManager, String username) throws SQLException {
        ResultSet rs = dbManager.query("SELECT COUNT(*) as row_count FROM  [Chat].[dbo].[FriendList] WHERE user_1=(SELECT user_id FROM dbo.Member_Credentials WHERE username='"+username+"');",CRUD_OPERATIONS.SELECT);

            rs.next();
            int row=rs.getInt("row_count");
            return row;

    }


    public static boolean queryIsFriendsWith(DBManager dbManager, String username1, String username2) throws SQLException {

        ResultSet rs = dbManager.query("SELECT COUNT(*) as row_count FROM  [Chat].[dbo].[FriendList] WHERE user_1=(SELECT user_id FROM dbo.Member_Credentials WHERE username='"+username1+"') and user_2=(SELECT user_id FROM dbo.Member_Credentials WHERE username='"+username2+"');",CRUD_OPERATIONS.SELECT);

            rs.next();
            int row=rs.getInt("row_count");

            if(row>0)
                return true;
            else
                return false;

    }

}
