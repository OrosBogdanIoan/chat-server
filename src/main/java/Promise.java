import java.util.concurrent.Future;

public class Promise <T,K> {
// T is what you wish, K is what you get
    private final T wish;
    private final  Future<K> results;


    public Promise(T wish, Future<K> results) {
        this.wish = wish;
        this.results = results;
    }

    public T getWish() {
        return wish;
    }

    public Future<K> getResults() {
        return results;
    }

    public boolean isFullfilled()
    {
       return results.isDone();

    }

}
